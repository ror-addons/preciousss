<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Preciousss" version="1.1" date="22/09/2008">
		<Author name="Wobin" email="wobster@gmail.com"/>
		<Description text="Shows extra tooltips when comparing jewelry and pocket items"/>
		<Files>
			<File name="Preciousss.xml"/>
		</Files>
		<Dependencies>
			<Dependency name="EASystem_Tooltips"/>
		</Dependencies>
		<OnInitialize>
			<CallFunction name="Preciousss.Setup"/>
		</OnInitialize>
		<OnShutdown/>
	</UiMod>
</ModuleFile>
