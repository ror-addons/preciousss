Preciousss = {}
local Slots = GameData.EquipSlots
local appropriateSlots = {	[Slots.POCKET1] = true, 
														[Slots.ACCESSORY1] = true } 

local howManyComparitors = {	[Slots.POCKET1] = 2, 
															[Slots.ACCESSORY1] = 4 }															

local pocketOrder = { [1] = Slots.POCKET1,
											[2] = Slots.POCKET2 }

local jewelOrder = {	[1] = Slots.ACCESSORY1,
											[2] = Slots.ACCESSORY2,
											[3] = Slots.ACCESSORY3,
											[4] = Slots.ACCESSORY4 }

local slotLists = { [Slots.ACCESSORY1] = jewelOrder,
										[Slots.POCKET1] = pocketOrder }

local TipChain = { "ItemComparisonTooltip", "ItemComparisonTooltip2", "ItemComparisonTooltip3", "ItemComparisonTooltip4", "ItemComparisonTooltip5", "ItemComparisonTooltip6", "ItemComparisonTooltip7", "ItemComparisonTooltip8", "ItemComparisonTooltip9" }										

Tooltips.ItemTooltip.COMPARISON_WIN_3		= TipChain[3]
Tooltips.ItemTooltip.COMPARISON_WIN_4		= TipChain[4]
Tooltips.ItemTooltip.COMPARISON_WIN_5		= TipChain[5]
Tooltips.ItemTooltip.COMPARISON_WIN_6		= TipChain[6]
Tooltips.ItemTooltip.COMPARISON_WIN_7		= TipChain[7]
Tooltips.ItemTooltip.COMPARISON_WIN_8		= TipChain[8]

local hookTooltip
local hookWindowChain
local floor = math.floor

function Preciousss.Setup()
	hookTooltip = Tooltips.CreateItemTooltip
	Tooltips.CreateItemTooltip = Preciousss.Tooltip

	hookWindowChain = Tooltips.AddExtraWindow
	Tooltips.AddExtraWindow = Preciousss.Linkerator

	for i=3,#TipChain,1 do
		local windowName = TipChain[i]
		TooltipWindowData[ windowName ] = { title = GetString( StringTables.Default.LABEL_CURRENT_ITEM ) }
		CreateWindow( windowName, false)
		LabelSetText( windowName.."Title", GetString( StringTables.Default.LABEL_CURRENT_ITEM ))
	end
end

function Preciousss.Tooltip(itemData, mouseoverWindow, anchor, disableComparison, extraText, extraTextColor, ignoreBroken)
	-- Now check if we're looking at a piece of jewelery or pocket item
	if not itemData or not appropriateSlots[itemData.equipSlot] then
		return hookTooltip(itemData, mouseoverWindow, anchor, disableComparison, extraText, extraTextColor, ignoreBroken)
	end
	-- Now call for jewelry and pocket items and -ignore- the comparison, we'll do it ourselves.
	hookTooltip(itemData, mouseoverWindow, anchor, true, extraText, extraTextColor, ignoreBroken) 

	-- Now we check the slots for any items to compare them to
	local count = 1
	local currentTip = TipChain[count]
	local currentAnchor = "ItemTooltip"

	for i,v in ipairs(TipChain) do
		WindowClearAnchors(v)
	end

	for i,slot in pairs(slotLists[itemData.equipSlot]) do
		local item = DataUtils.GetEquipmentData()[slot]
		if mouseoverWindow ~= "CharacterWindowContentsEquipmentSlot"..slot then
			if item and item.id ~= 0 then
				Tooltips.SetItemTooltipData(currentTip, item)
				Tooltips.AddExtraWindow(currentTip, currentAnchor, item)
				count = count + 1
				currentTip = TipChain[count]
				currentAnchor = TipChain[count - 1]
			end
		end
	end
end

local function GetOppositeCorners(window)
	local x,y = WindowGetScreenPosition(window)
	x = x / InterfaceCore.GetScale()
	y = y / InterfaceCore.GetScale()

	l, h = WindowGetDimensions(window)
	return floor(x), floor(y), floor(x + l), floor(y + h)
end


-- Currently the methodology for chaining tooltips is "attach to the rightmost
-- side if it fits on the screen, otherwise the leftmostside.", however, this
-- trips up if there's no room on either side left (viewable in lower resolution
-- layouts) So we need to make this logic a bit more flexible.
function Preciousss.Linkerator(windowName, windowToAnchorTo, extraData)
	hookWindowChain(windowName, windowToAnchorTo, extraData)

	local relativeAnchor, anchor, parent = WindowGetAnchor(windowName, 1)
	local x1, y1, x2, _ = GetOppositeCorners(parent)
	local xa, _, xb, _  = GetOppositeCorners(windowName)
	local _, rootHeight = WindowGetDimensions ("Root")
	
	-- Basically what we're doing here is "If the current tooltip is squashed into
	-- the parent one, move it above the parent tooltip if the parent tooltip is
	-- in the lower half of the screen, otherwise move it below the parent
	-- tooltip. The current code deals with left or right of the mouse cursor.

	if	(anchor == "topleft" and relativeAnchor=="topright") or 
			(anchor == "topright" and relativeAnchor == "topleft") then
			if	(anchor == "topleft" and xa < x2) or
					(anchor == "topright" and xb > x1) then
				-- we're squashed
				WindowClearAnchors(windowName)
				if y1 >	rootHeight/2 then
					WindowAddAnchor(windowName, "topleft", parent, "bottomleft", 0, 0) 
				else
					WindowAddAnchor(windowName, "bottomleft", parent, "topleft", 0, 0) 
				end
		end
	end
end

